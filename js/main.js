window.onload = function () {
  // обработка открытия закрытия мобильного меню

  function mobile_menu(e) {
    try {
      mobileOpenId = "mobile-menu-open";
      mobileCloseId = "mobile-menu-close";
      mobileMenuClass = "mobile-menu";
      mobileMenuClassOpen = mobileMenuClass + "_open";
      mobileMenu = document.getElementsByClassName(mobileMenuClass)[0];
      if (
        !e.target.classList.contains(mobileMenuClass) ||
        e.target.id == mobileCloseId
      ) {
        mobileMenu.classList.remove(mobileMenuClassOpen);
      }
      if (e.target.id == mobileOpenId) {
        mobileMenu.classList.add(mobileMenuClassOpen);
      }
    } catch (error) {}
  }

  // выпадающие списки меню
  function sub_menu(e) {
    try {
      subMenuClass = "sub-menu";
      subMenuClassOpen = subMenuClass + "_open";

      if (e.target.parentElement.classList.contains(subMenuClass)) {
        var elem = e.target.parentElement;
        if (elem.classList.contains(subMenuClassOpen)) {
          elem.classList.remove(subMenuClassOpen);
        } else {
          elem.classList.add(subMenuClassOpen);
        }
      } else {
        // убираем все выпадающие подменю
        var elements = document.getElementsByClassName(subMenuClass);
        for (let i = 0; i < elements.length; i += 1) {
          elements[i].classList.remove(subMenuClassOpen);
        }
      }
    } catch (error) {}
  }
  // выпадающие списки вкладок продуктов
  function dropdown_control(e) {
    try {
      dropdownClassClick = "dropdown-block__title";
      dropdownClassOpen = "dropdown-block_open";
      if (e.target.parentElement.classList.contains(dropdownClassClick)) {
        var elem = e.target.parentElement.parentElement.parentElement;
        if (elem.classList.contains(dropdownClassOpen)) {
          elem.classList.remove(dropdownClassOpen);
        } else {
          elem.classList.add(dropdownClassOpen);
        }
      }
    } catch (error) {}
  }

  document.onclick = function (e) {
    mobile_menu(e);
    sub_menu(e);
    dropdown_control(e);
  };
};

try {
  var map = document.getElementById("map");
  map.addEventListener(
    "load",
    function () {
      var svgDoc = map.contentDocument;
      var tooltipIdArr = ["city1", "city2", "city3", "city4"];
      for (let i = 0; i < tooltipIdArr.length; i += 1) {
        var tooltip = svgDoc.getElementById(tooltipIdArr[i]);
        var tooltipBlock = document.getElementById(
          "tooltip-" + tooltipIdArr[i]
        );
        if (tooltip && tooltipBlock) {
          tooltip.addEventListener(
            "mouseover",
            function (e) {
              tooltip.style.cursor = "pointer";
              tooltipBlock.style.left = e.pageX + "px";
              tooltipBlock.style.top = e.pageY + "px";
              tooltipBlock.classList.add("tooltip_show");
            },
            false
          );
          tooltip.addEventListener(
            "mouseout",
            function () {
              tooltipBlock.classList.remove("tooltip_show");
            },
            false
          );
        }
      }
    },
    false
  );
} catch (error) {}
